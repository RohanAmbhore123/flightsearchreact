import React, { Component } from 'react';

import OneWay from '../../components/OneWay/OneWay';
import Aux from '../../hoc/Auxi/Aux';
import './search-home.scss';
import DetailHead from '../../components/DetailHead/DetailHead';

class SearchHome extends Component {
    render() {
        return (
            <div className='search-home'>
                <div className='container-fluid'>
                    <div className="row">
                        <div className="col-sm-3" style={{ background: 'yellow' }}>
                            <OneWay />
                        </div>
                        <div className="col-sm-9" style={{ background: 'orange' }}>
                            <DetailHead />
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchHome;