import React, {Component} from'react';

import Aux from './hoc/Auxi/Aux';
import Layout from './hoc/Layout/Layout';
import SearchHome from './containers/SearchHome/SearchHome';

class App extends Component {
    render() {
        return (
            <Aux>
                <Layout>
                    <SearchHome />
                </Layout>
            </Aux>
        );
    }
}

export default App;