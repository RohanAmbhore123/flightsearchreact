import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from 'react-router-dom'; 


import './index.scss';
import App from './App.js';

const app = (
    <BrowserRouter>
        <App/>
    </BrowserRouter>
);

ReactDom.render(app, document.getElementById('root'));