import React, { Component } from 'react';

class OneWay extends Component {
    state = {
        flightForm: {
            origin: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Origin City'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            destnation: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Destination City'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            date: {
                elementType: 'date',
                elementConfig: {
                    type: 'date',
                    placeholder: 'Departure date'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            }
        },
        formIsValid: false
    }
    render() {
        const formElementsArray = [];
        for (let key in this.state.flightForm) {
            formElementsArray.push({
                id: key,
                config: this.state.flightForm[key]
            });
        }
        let form  = (
            <form>
                {formElementsArray.map(formElement => {
                   <Input />
                })}
            </form>
        );
        return (
            <div>
            {form}
            <p>Form </p>
            </div>
        );
    }
}

export default OneWay;