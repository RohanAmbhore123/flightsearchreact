import React, { Component } from 'react';

import Aux from '../Auxi/Aux';
import Toolbar from '../../components/Navigations/ToolBar/ToolBar';

class Layout extends Component {
    render() {
        return (
            <Aux>
                <Toolbar />
                <main>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

export default Layout;